package ru.t1.godyna.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.godyna.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.godyna.tm.api.service.dto.ISessionDtoService;
import ru.t1.godyna.tm.dto.model.SessionDTO;
import ru.t1.godyna.tm.exception.entity.EntityNotFoundException;
import ru.t1.godyna.tm.exception.entity.UserNotFoundException;
import ru.t1.godyna.tm.exception.field.IdEmptyException;
import ru.t1.godyna.tm.exception.field.UserIdEmptyException;
import ru.t1.godyna.tm.exception.user.AccessDeniedException;

import java.util.List;

@Service
@NoArgsConstructor
public class SessionDtoService implements ISessionDtoService {

    @NotNull
    @Autowired
    private ISessionDtoRepository sessionRepository;

    @NotNull
    @Override
    @Transactional
    public SessionDTO add(@Nullable SessionDTO model) {
        if (model == null) throw new AccessDeniedException();
        sessionRepository.add(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO add(@Nullable String userId, @Nullable SessionDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new AccessDeniedException();model.setUserId(userId);
        sessionRepository.add(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public List<SessionDTO> add(@NotNull List<SessionDTO> models) {
        if (models == null) throw new EntityNotFoundException();
        for (@NotNull SessionDTO model : models) {
            sessionRepository.add(model);
        }
        return models;
    }

    @Override
    @Transactional
    public void clear(@NotNull String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        sessionRepository.removeByUserId(userId);
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return findOneById(id) != null;
    }

    @Override
    public @Nullable List<SessionDTO> findAll() {
        return sessionRepository.findAll();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return sessionRepository.findOneById(id);

    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return sessionRepository.findOneByIdUserId(userId, id);
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO update(@Nullable SessionDTO model) {
        if (model == null) throw new AccessDeniedException();
        sessionRepository.update(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO remove(@Nullable SessionDTO model) {
        if (model == null) throw new AccessDeniedException();
        sessionRepository.remove(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO session = findOneById(id);
        if (session == null) throw new AccessDeniedException();
        sessionRepository.remove(session);
        return session;
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO session = findOneById(userId, id);
        if (session == null) throw new AccessDeniedException();
        sessionRepository.remove(session);
        return session;
    }

}
